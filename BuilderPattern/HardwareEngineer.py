from typing import Optional, Union

from BuilderPattern.Computer.Computer import Computer
from BuilderPattern.Computer.ComputerBuilder import ComputerBuilder
from BuilderPattern.Tablet.Tablet import Tablet
from BuilderPattern.Tablet.TabletBuilder import TabletBuilder


class HardwareEngineer:

    def __init__(self):
        self.__computer_builder: Optional[ComputerBuilder] = None
        self.__tablet_builder: Optional[TabletBuilder] = None

    """Device constructions"""
    def construct_computer(self, memory_amount, hdd_amount, gpu_model):
        self.__computer_builder = ComputerBuilder()

        # Configuration of computer
        self.__computer_builder.configure_memory(memory_amount)
        self.__computer_builder.configure_hdd(hdd_amount)
        self.__computer_builder.configure_gpu(gpu_model)

    def construct_tablet(self, memory_amount, hdd_amount, touchpad_model):
        self.__tablet_builder = TabletBuilder()

        # Configuration of tablet
        self.__tablet_builder.configure_memory(memory_amount)
        self.__tablet_builder.configure_hdd(hdd_amount)
        self.__tablet_builder.configure_touchpad(touchpad_model)

    """Device properties"""
    @property
    def computer(self) -> Union[Computer, str]:
        if self.__computer_builder is None:
            return 'You should first construct computer'
        return self.__computer_builder.computer

    @property
    def tablet(self) -> Union[Tablet, str]:
        if self.__tablet_builder is None:
            return 'You should first construct tablet'
        return self.__tablet_builder.tablet
