from BuilderPattern.HardwareEngineer import HardwareEngineer


def main() -> None:
    engineer = HardwareEngineer()
    # engineer.construct_computer(memory_amount=16,
    #                             hdd_amount=1024,
    #                             gpu_model="GeForce GTX 154 GU")
    #
    # computer = engineer.computer
    engineer.construct_tablet(memory_amount=8,
                              hdd_amount=500,
                              touchpad_model='Elan')

    tablet = engineer.tablet
    print(tablet)


if __name__ == '__main__':
    main()
