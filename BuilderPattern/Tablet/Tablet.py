from typing import Optional


class Tablet:
    def __init__(self, serial_number: str):
        self.serial_number = serial_number
        self.memory: Optional[int] = None  # In gigabytes
        self.hdd: Optional[int] = None  # In gigabytes
        self.touchpad: Optional[str] = None

    def __str__(self):
        info = (f'Memory: {self.memory}GB',
                f'Hard disk: {self.hdd}GB',
                f'Touchpad: {self.touchpad}')

        return '\n'.join(info)
