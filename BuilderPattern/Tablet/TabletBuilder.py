from BuilderPattern.Tablet.Tablet import Tablet
from BuilderPattern.Utilities.IdGenerator import IdGenerator


class TabletBuilder:
    def __init__(self):
        new_id = IdGenerator.generate()
        self.tablet = Tablet(new_id)

    def configure_memory(self, amount: int):
        self.tablet.memory = amount

    def configure_hdd(self, amount: int):
        self.tablet.hdd = amount

    def configure_touchpad(self, model: str):
        self.tablet.touchpad = model
