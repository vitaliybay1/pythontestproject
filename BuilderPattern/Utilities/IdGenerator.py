import random
import string

from typing import Optional


class IdGenerator:

    @staticmethod
    def generate(length: Optional[int] = 10) -> str:
        return ''.join(random.choice(string.ascii_letters + string.digits) for n in range(length))
