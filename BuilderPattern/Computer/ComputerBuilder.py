from BuilderPattern.Computer.Computer import Computer
from BuilderPattern.Utilities.IdGenerator import IdGenerator


class ComputerBuilder:
    def __init__(self):
        new_id = IdGenerator.generate()
        self.computer = Computer(new_id)

    def configure_memory(self, amount: int):
        self.computer.memory = amount

    def configure_hdd(self, amount: int):
        self.computer.hdd = amount

    def configure_gpu(self, model: str):
        self.computer.gpu = model
