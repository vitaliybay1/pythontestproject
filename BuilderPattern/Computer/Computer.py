from typing import Optional


class Computer:
    def __init__(self, serial_number: str):
        self.serial_number: str = serial_number
        self.memory: Optional[int] = None  # in gigabytes
        self.hdd: Optional[int] = None  # in gigabytes
        self.gpu: Optional[str] = None

    def __str__(self):
        info = (f'Memory: {self.memory}GB',
                f'Hard disk: {self.hdd}GB',
                f'Graphics Card: {self.gpu}')

        return '\n'.join(info)
