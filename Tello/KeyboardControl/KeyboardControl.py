from time import sleep

from djitellopy import tello

from Tello.KeyboardControl.KeyboardControlMain import get_key, init

init()

drone = tello.Tello()
drone.connect()
print(drone.get_battery())

def get_keyboard_input():
    lr, fb, ud, yv = 0, 0, 0, 0
    speed = 50
    if get_key('UP'):
        print('UP')
    if get_key('DOWN'):
        print('DOWN')
    # if get_key('LEFT'): lr = -speed
    # elif get_key('RIGHT'): lr = speed
    #
    # if get_key('UP'): fb = speed
    # elif get_key('DOWN'): fb = -speed
    #
    # if get_key('w'): ud = speed
    # elif get_key('s'): ud = -speed
    #
    # if get_key('a'): yv = -speed
    # elif get_key('d'): yv = speed
    #
    # if get_key('l'): drone.land()
    # if get_key('t'): drone.takeoff()

    return [lr, fb, ud, yv]


while True:
    values = get_keyboard_input()
    drone.send_rc_control(values[0], values[1], values[2], values[3])
    sleep(0.05)
