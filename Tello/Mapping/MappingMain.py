import math
from time import sleep

import cv2
import numpy as np
from djitellopy import tello

from Tello.KeyboardControl.KeyboardControlMain import get_key, init

############## Parameters ################
x, y = 300, 590
angle = 0
yaw = 0

forward_speed = 150/10  # in cm/s (15cm/s)
angular_speed = 80/10  # in degrees/s
interval = 0.25

distance_interval = forward_speed * interval
angular_interval = angular_speed * interval
##########################################

init()

drone = tello.Tello()
drone.connect()
print(drone.get_battery())

points = [(0, 0), (0, 0)]

def get_keyboard_input():
    global x, distance_interval, angle, yaw, y, interval, forward_speed, angular_speed
    lr, fb, ud, yv = 0, 0, 0, 0
    speed = forward_speed
    aspeed = angular_speed
    distance = 0

    if get_key('LEFT'):
        lr = -int(speed * 2)
        distance = distance_interval
        angle = -180

    elif get_key('RIGHT'):
        lr = int(speed * 2)
        distance = -distance_interval
        angle = 180

    if get_key('UP'):
        fb = int(speed * 2)
        print(fb)
        distance = distance_interval
        angle = 270
    elif get_key('DOWN'):
        fb = -int(speed * 2)
        distance = -distance_interval
        angle = -90

    if get_key('w'):
        ud = int(speed * 2)
    elif get_key('s'):
        ud = -int(speed * 2)

    if get_key('a'):
        yv = -int(aspeed * 5)
        yaw -= angular_speed

    elif get_key('d'):
        yv = int(aspeed * 5)
        yaw += angular_speed

    if get_key('l'): drone.land()
    if get_key('t'): drone.takeoff()

    sleep(interval)
    angle += yaw
    x += int(distance * math.cos(math.radians(angle)))
    y += int(distance * math.sin(math.radians(angle)))

    return [lr, fb, ud, yv, x, y]

def draw_points(img, points):
    [cv2.circle(img, point, 5, (0, 0, 255), cv2.FILLED) for point in points]
    cv2.circle(img, points[-1], 8, (0, 255, 0), cv2.FILLED)
    cv2.putText(img,
                f'({(points[-1][0] - 300) / 100 }, {(points[-1][1] - 590) / 100})',
                (points[-1][0] + 10, points[-1][1] + 10), cv2.FONT_HERSHEY_PLAIN, 1, (255, 0, 255), 1)

while True:
    values = get_keyboard_input()
    drone.send_rc_control(values[0], values[1], values[2], values[3])

    img = np.zeros((600, 600, 3), np.uint8)
    if points[-1][0] != values[4] or points[-1][1] != values[5]:
        points.append((values[4], values[5]))
    draw_points(img, points)
    cv2.imshow("Test", img)
    cv2.waitKey(1)
