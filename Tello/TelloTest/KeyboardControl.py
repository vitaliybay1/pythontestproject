import pygame


class KeyboardEventControl:
    def __init__(self):
        pygame.init()
        win = pygame.display.set_mode((400, 400))

    def get_key(self, key_name: str) -> bool:
        answer = False
        for e in pygame.event.get():
            pass
        key_pressed = pygame.key.get_pressed()
        key = getattr(pygame, 'K_{}'.format(key_name))
        if key_pressed[key]:
            answer = True
        pygame.display.update()
        return answer
