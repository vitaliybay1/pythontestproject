from time import sleep
from typing import Any

import cv2
import numpy as np
from djitellopy import tello

from Tello.TelloTest.KeyboardControl import KeyboardEventControl


class TelloTestData:
    KEY_PRESS_INTERVAL = 0.095
    keyboard_control: KeyboardEventControl
    drone: tello.Tello
    WIDTH: int = 600
    HEIGHT: int = 600
    SPEED = 25
    IMG: Any
    INTERVAL: float = 0.25
    x: int = WIDTH / 2
    y: int = WIDTH / 2

    def __init__(self):
        self.drone = tello.Tello()
        self. drone.connect()
        self.keyboard_control = KeyboardEventControl()
        self.IMG = np.zeros((self.WIDTH, self.HEIGHT, 3), np.uint8)
        self.draw_points()
        cv2.imshow('Test', self.IMG)
        cv2.setMouseCallback('Test', self.click_event)
        cv2.waitKey(1)


    def click_event(self, event, x, y, flags, params):
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.putText(self.IMG, f'{x}, {y}', (x, y), font, 1, (255, 0, 0), 2)
        # cv2.imshow('Test', self.IMG)
        # cv2.waitKey(2)

        if event == cv2.EVENT_LBUTTONDOWN:
            print('click event')
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(self.IMG, f'{x}, {y}', (x, y), font, 1, (0, 255, 0), 2)
            cv2.circle(self.IMG, (x, y), 6, (0, 255, 0), cv2.FILLED)
            cv2.imshow('Test', self.IMG)
            cv2.waitKey(2)
            while self.x != x or self.y != y:
                print('click event while')
                sub_x = int(x - (int(self.SPEED * self.INTERVAL)))
                sup_x = int(x + (int(self.SPEED * self.INTERVAL)))
                if sub_x < int(self.x) < sup_x:
                    self.x = x
                    self.drone.send_rc_control(0, 0, 0, 0)
                    sleep(self.INTERVAL)
                elif x > self.x:
                    self.x = int(self.x + (int(self.SPEED * self.INTERVAL)))
                    self.drone.send_rc_control(20, 0, 0, 0)
                    sleep(self.INTERVAL)
                else:
                    self.x = int(self.x - (int(self.SPEED * self.INTERVAL)))
                    self.drone.send_rc_control(-20, 0, 0, 0)
                    sleep(self.INTERVAL)

                sub_y = int(y - (int(self.SPEED * self.INTERVAL)))
                sup_y = int(y + (int(self.SPEED * self.INTERVAL)))
                if sub_y < int(self.y) < sup_y:
                    self.y = y
                    self.drone.send_rc_control(0, 0, 0, 0)
                    sleep(self.INTERVAL)
                elif y > self.y:
                    self.y = int(self.y + (int(self.SPEED * self.INTERVAL)))
                    self.drone.send_rc_control(0, 20, 0, 0)
                    sleep(self.INTERVAL)
                else:
                    self.y = int(self.y - (int(self.SPEED * self.INTERVAL)))
                    # self.drone.send_rc_control(0, -20, 0, 0)
                    print('0, -20, 0, 0')
                    sleep(self.INTERVAL)
                cv2.circle(self.IMG, (self.x, self.y), 6, (255, 0, 0), cv2.FILLED)
                cv2.imshow('Test', self.IMG)
                cv2.waitKey(2)
                sleep(self.INTERVAL)

    def __bounded_keys(self):
        print('keyboard event')
        move = False
        left_right, forward_backward, up_down, yaw = 0, 0, 0, 0
        if self.keyboard_control.get_key('UP'):
            move = True
            forward_backward = self.SPEED
            self.x += int(self.SPEED * self.INTERVAL)
            print('UP')
        elif self.keyboard_control.get_key('DOWN'):
            move = True
            forward_backward = -self.SPEED
            self.x += int(-(self.SPEED * self.INTERVAL))
            print('DOWN')
        if self.keyboard_control.get_key('LEFT'):
            move = True
            left_right = -self.SPEED
            self.y += int(-(self.SPEED * self.INTERVAL))
            print('LEFT')
        if self.keyboard_control.get_key('RIGHT'):
            move = True
            left_right = self.SPEED
            self.y += int((self.SPEED * self.INTERVAL))
            print('RIGHT')
        if self.keyboard_control.get_key('w'):
            move = True
            print('w')
        if self.keyboard_control.get_key('s'):
            move = True
            print('s')
        if self.keyboard_control.get_key('a'):
            move = True
            print('a')
        if self.keyboard_control.get_key('d'):
            move = True
            print('d')
        if self.keyboard_control.get_key('t'):
            self.drone.takeoff()
            print('t')
        if self.keyboard_control.get_key('l'):
            self.drone.land()
            print('l')
        if self.keyboard_control.get_key('1'):
            print(self.drone.get_battery())
            print('1')
        if self.keyboard_control.get_key('2'):
            # print(self.drone.get_barometer())
            print('2')
        if self.keyboard_control.get_key('3'):
            print('3')
        if self.keyboard_control.get_key('4'):
            print('4')
        if self.keyboard_control.get_key('5'):
            print('5')
        if self.keyboard_control.get_key('6'):
            print('6')

        sleep(self.INTERVAL)
        return move, left_right, forward_backward, up_down, yaw

    def draw_points(self):
        cv2.circle(self.IMG, (int(self.x), int(self.y)), 6, (255, 0, 0), cv2.FILLED)

    def register_pressed_keys(self):
        while True:
            print('keyboard event while')
            move, left_right, forward_backward, up_down, yaw = self.__bounded_keys()
            if move:
                # self.drone.send_rc_control(left_right, forward_backward, up_down, yaw)
                print(left_right, forward_backward, up_down, yaw)
                # self.IMG = np.zeros((self.WIDTH, self.HEIGHT), np.uint8)
                self.draw_points()
                cv2.imshow('Test', self.IMG)
                cv2.waitKey(2)
                ###### DATA REGISTRATION #################
                # print(f'Speed X: {self.drone.get_speed_x()}')
                # print(f'Acceleration X: {self.drone.get_acceleration_x()}')
                # print(f'Speed Y: {self.drone.get_speed_y()}')
                # print(f'Acceleration Y: {self.drone.get_acceleration_y()}')
                # print(f'Height: {self.drone.get_height()}')


            sleep(self.KEY_PRESS_INTERVAL)


if __name__ == '__main__':
    test = TelloTestData()
    test.register_pressed_keys()
