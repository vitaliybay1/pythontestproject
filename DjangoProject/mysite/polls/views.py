from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def index(request):
    return HttpResponse("Hello World!!!")

def sum(request, num1:int, num2:int) -> HttpResponse:
    return HttpResponse(f"Sum is: {num1 + num2}")
