from django.urls import path

from . import views

app_name="onlineVotation"
urlpatterns = [
    path('', views.indexPage, name="index"),
    path('habitant', views.habitantPage, name="habitant"),
    path('organizer', views.oraganizerPage, name="organizer"),
    path('admin', views.adminPage, name="admin"),
    path('newUser', views.newUserPage, name="newUser"),
    path('registerNewUser', views.registerNewUser, name="registerNewUser"),
    path('updateProvince/<int:id>', views.update_provinces, name="updateProvince")
]