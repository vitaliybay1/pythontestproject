function updateProvinces(e) {
    console.log(e.value)
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:8000/updateProvince/' + e.value, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        let status = xhr.status;
        console.log(status)
        if (status === 200) {
            console.log(xhr.response);
            let provinceSelector = document.getElementById('id_provinces');
            provinceSelector.innerHTML = "";
            for (let i = 0; i < xhr.response.length; i++) {
                let option = document.createElement('option');
                option.value = xhr.response[i].id;
                option.innerHTML = xhr.response[i].name;
                provinceSelector.appendChild(option);
            }
        } else {
            console.log(xhr.response);
        }
    };
    xhr.send();
}