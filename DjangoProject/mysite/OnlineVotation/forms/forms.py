from django import forms

class NameForm(forms.Form):

    def update_provinces(self, model_province):
        self.fields["provinces"].choices = model_province

    def update_a_c(self, model_ac):
        self.fields["autonomous_community"].choices = model_ac



    name = forms.CharField(label='Name:',
                                 max_length=100,
                                 required=True,
                                 widget=forms.TextInput(attrs={"placeholder" : "Name"}))
    sur_name = forms.CharField(label='Sur Name:',
                                 max_length=100,
                                 required=True,
                                 widget=forms.TextInput(attrs={"placeholder" : "SurName"}))
    phone_number = forms.IntegerField(label='Phone number:',
                                      required=True,
                                      widget=forms.NumberInput(attrs={"placeholder" : "Phone Number"}))
    email = forms.EmailField(label='Email',
                             required=True,
                             widget=forms.EmailInput(attrs={"placeholder" : "Email"}))

    autonomous_community = forms.ChoiceField(label='Autonomous Community',
                                       required=True,
                                       choices=[],
                                  widget=forms.Select(attrs={"class" : "selector", "onchange" : "updateProvinces(this)"}))

    provinces = forms.ChoiceField(label='Provinces',
                                       required=True,
                                       choices=[],
                                  widget=forms.Select(attrs={"class" : "selector"}))

    birthday = forms.DateField(label='Birthday',
                               required=True,
                               widget=forms.DateInput(attrs={"placeholder" : "Birthday"}),
                               input_formats=['%m/%d/%Y', '%m/%d/%y'])
    password = forms.CharField(label='Password',
                               required=True,
                               widget=forms.PasswordInput(attrs={"placeholder" : "Password"}))
    repeat_password = forms.CharField(label='Repeat Password',
                               required=True,
                               widget=forms.PasswordInput(attrs={"placeholder" : "Repeat Password"}))




