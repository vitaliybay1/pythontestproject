import json

from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse


# Create your views here.
from .forms.forms import NameForm


def indexPage(request):
    return render(request, "OnlineVotation/home/home.html")


def habitantPage(request):
    autorized = False
    if not autorized:
        return render(request, "OnlineVotation/habitant/habitantLoginPage.html")
    return render(request, "OnlineVotation/habitant/habitant.html")


def oraganizerPage(request):
    autorized = False
    if not autorized:
        return render(request, "OnlineVotation/shared/commonLoginPage.html")
    return render(request, "OnlineVotation/organizer/organizer.html")


def adminPage(request):
    autorized = False
    if not autorized:
        return render(request, "OnlineVotation/shared/commonLoginPage.html")
    return render(request, "OnlineVotation/admin/admin.html")

def newUserPage(request):
    form = NameForm()
    form.update_provinces([("1", "Malaga"),("2", "Sevilla"),("3", "Granada")])
    form.update_a_c([("1", "Andalucia"),("2", "Galicia"),("3", "Canarias")])
    return render(request, "OnlineVotation/newUser/newUser.html", {"form" : form})

def registerNewUser(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        form.update_provinces([("1", "Malaga"),("2", "Sevilla"),("3", "Granada")])
        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
        else:
            return render(request, "OnlineVotation/newUser/newUser.html", {"form" : form, "errors" : form.errors})
    return HttpResponse('')

class Province:
    id: int
    name: str

    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


def update_provinces(request, id):
    if id == 1:
        result = [Province("1", "A"), Province("2", "A"), Province("3", "A")]
    elif id == 2:
        result = [Province("1", "B"), Province("2", "B"), Province("3", "B")]
    elif id == 3:
        result = [Province("1", "C"), Province("2", "C"), Province("3", "C")]

    return HttpResponse(json.dumps([ob.__dict__ for ob in result]), content_type="application/json")