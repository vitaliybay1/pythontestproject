from django.apps import AppConfig


class OnlinevotationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'OnlineVotation'
