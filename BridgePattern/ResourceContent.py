class ResourceContent:
    def __init__(self, imp):
        self.imp = imp

    def show_content(self, path):
        return self.imp.fetch(path)
