from BridgePattern.Fetchers.FileFetcher import FileFetcher
from BridgePattern.Fetchers.URLFetcher import URLFetcher
from BridgePattern.ResourceContent import ResourceContent

if __name__ == "__main__":
    url_fetcher = URLFetcher()
    iface = ResourceContent(url_fetcher)
    content = iface.show_content("http://python.org")
    print(content)

    print("##########################################")
    file_fetcher = FileFetcher()
    iface = ResourceContent(file_fetcher)
    content = iface.show_content("test.txt")
    print(content)
