import urllib.request


class URLFetcher:

    def fetch(self, path):
        req = urllib.request.Request(path)
        with urllib.request.urlopen(req) as response:
            the_page = response.read()
            return the_page
