class FileFetcher:

    def fetch(self, path):
        with open(path) as file:
            return file.read()