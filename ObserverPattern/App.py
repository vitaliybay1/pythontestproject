from ObserverPattern.ApiService.UserApi.user import registerNewUser, passwordForgotten
from ObserverPattern.Listener.LogHandler.LogListener import setupLogEventHandler
from ObserverPattern.Listener.MailHandler.MailListener import setupEmailEventHandler

# Initialize event listener
setupLogEventHandler()
setupEmailEventHandler()

# Register a new user
registerNewUser("Vitaliy", "testPWD", "textVit@gmail.com")

# Send a password reset message
passwordForgotten("textVit@gmail.com")

