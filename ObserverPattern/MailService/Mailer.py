def sendEmail(fromAddress: str, to: str, subject: str, body: str) -> None:
    print(f"From {fromAddress} to {to} \nSubject: {subject} \n{body}")