from ObserverPattern.InternServcies.Hasher.Hasher import hashString

class User:
    def __init__(self, name: str, password: str, email: str):
        self.name: str = name
        self.password: str = hashString(password)
        self.email: str = email
        self.plan: str = "basic"
        self.resetCode: str = ""

    def __repr__(self):
        return f"User(name={self.name}, password={self.password}, email={self.email}"

    def resetPassword(self, code: str, newPassword: str):
        if code != self.resetCode:
            raise Exception("Invalid password reset code.")
        self.password = hashString(newPassword)
