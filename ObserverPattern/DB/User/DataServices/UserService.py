from ObserverPattern.DB.User.Models.Public.User import User

users = []

def createUser(name: str, password: str, email: str) -> User:
    newUser = User(name, password, email)
    users.append(newUser)
    return newUser

def findUserByEmail(email: str) -> User:
    for user in users:
        if user.email == email:
            return user
    raise Exception(f"User with email address {email} not found")

