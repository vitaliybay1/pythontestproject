from hashlib import blake2b

def hashString(textToHash: str) -> str:
    return blake2b(textToHash.encode('UTF8')).hexdigest()
