import random
import string

def getRandomString(length: int):
    return ''.join(random.choice(string.ascii_letters) for i in range(length))

