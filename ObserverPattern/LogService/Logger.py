import datetime


def log(dateTime: datetime, event: str):
    print(f"{dateTime} - {event}")
