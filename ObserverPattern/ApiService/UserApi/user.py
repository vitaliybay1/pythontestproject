from ObserverPattern.DB.User.DataServices.UserService import createUser, findUserByEmail
from ObserverPattern.InternServcies.RandomGenerator.RandomStringGenerator import getRandomString
from ObserverPattern.Listener.Event import EventTypes, postEvent


def registerNewUser(name: str, password: str, email: str):
    user = createUser(name, password, email)

    postEvent(EventTypes.UserRegistrated.value, user)

def passwordForgotten(email: str):
    user = findUserByEmail(email)
    user.resetCode = getRandomString(9)

    postEvent(EventTypes.ResetPasswordCodeGenerated.value, user)

