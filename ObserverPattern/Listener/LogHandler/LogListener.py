from datetime import datetime
from ObserverPattern.DB.User.Models.Public import User
from ObserverPattern.Listener.Event import subscribe, EventTypes
from ObserverPattern.LogService.Logger import log


def handleUserRegistrationEvent(user: User):
    log(datetime.now(), f"User with email {user.email} has been created")

def handleGenerateResetPasswordCodeEvent(user: User):
    log(datetime.now(), f"Generated reset code for {user.email} user")

def setupLogEventHandler():
    subscribe(EventTypes.UserRegistrated.value, handleUserRegistrationEvent)
    subscribe(EventTypes.ResetPasswordCodeGenerated.value, handleGenerateResetPasswordCodeEvent)
