from enum import Enum


class EventTypes(Enum):
    UserRegistrated = "UserRegistrated",
    ResetPasswordCodeGenerated = "ResetPasswordCodeGenerated"

subscribedEvents = dict()

def subscribe(eventType: str, fn):
    if eventType not in subscribedEvents:
        subscribedEvents[eventType] = []
    subscribedEvents[eventType].append(fn)

def postEvent(eventType: str, data):
    if eventType not in subscribedEvents:
        return
    for fn in subscribedEvents[eventType]:
        fn(data)

