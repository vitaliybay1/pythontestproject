from ObserverPattern.DB.User.Models.Public import User
from ObserverPattern.Listener.Event import subscribe, EventTypes
from ObserverPattern.MailService.Mailer import sendEmail


def handleUserRegistrationEvent(user: User):
    sendEmail("observerPatter@email.com", user.email, "New User", f"User with email {user.email} has been created")

def handleGenerateResetPasswordCodeEvent(user: User):
    sendEmail("observerPatter@email.com", user.email, "Reset password", f"For reset password you should use {user.resetCode} code")

def setupEmailEventHandler():
    subscribe(EventTypes.UserRegistrated.value, handleUserRegistrationEvent)
    subscribe(EventTypes.ResetPasswordCodeGenerated.value, handleGenerateResetPasswordCodeEvent)
