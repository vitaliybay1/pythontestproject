class LazyProperty:

    def __init__(self, method):
        self.method = method
        self.method_name = method.__name__
        print(f'function overriden: {self.method}')
        print(f'function\'s name: {self.method_name}')

    def __get__(self, instance, owner):
        if not instance:
            return None
        value = self.method(instance)
        print(f'Value {value}')
        setattr(instance, self.method_name, value)
        return value
