from pygments.lexer import default

from SimpleObserverPattern.Publishers.DefaultFormatter import DefaultFormatter
from SimpleObserverPattern.Observers.HexFormatterObserver import HexFormatterObserver
from SimpleObserverPattern.Observers.BinaryFormatterObserver import BinaryFormatterObserver

def main():
    default_formatter = DefaultFormatter("DF")
    print(default_formatter)
    print()

    hex_formatter = HexFormatterObserver()
    default_formatter.add(hex_formatter)
    default_formatter.data = 5
    print(default_formatter)
    print()

    binary_formatter = BinaryFormatterObserver()
    default_formatter.add(binary_formatter)
    default_formatter.data = 112
    print(default_formatter)
    print()

    print("------------------------------------------")
    print()

    default_formatter.remove(hex_formatter)
    default_formatter.data = 543
    print(default_formatter)
    print()

    default_formatter.remove(hex_formatter)
    print()

    default_formatter.add(binary_formatter)
    print()

    default_formatter.data = 'stringData'
    default_formatter.data = 12.2
    print()
    print(default_formatter)


if __name__ == "__main__":
    main()
