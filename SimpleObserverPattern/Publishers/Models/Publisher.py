from SimpleObserverPattern.Observers.Models.Observer import Observer

class Publisher:
    name: str
    observers: [Observer]
    _data: int

    def __init__(self):
        self.observers = []

    def add(self, observer: Observer):
        if observer not in self.observers:
            self.observers.append(observer)
        else:
            print(f'Observer already exists')

    def remove(self, observer: Observer):
        if observer in self.observers:
            self.observers.remove(observer)
        else:
            print(f'Observer does not exists in list')

    def notify(self):
        [observer.notify(self) for observer in self.observers]

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, new_value: int):
        try:
            self._data = int(new_value)
        except ValueError as e:
            print(f'Error: {str(e)}')
        else:
            self.notify()
