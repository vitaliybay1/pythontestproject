from .Models.Publisher import Publisher

class DefaultFormatter(Publisher):
    def __init__(self, name: str):
        Publisher.__init__(self)
        self.name = name
        self._data = 0

    def __str__(self):
        return f'{type(self).__name__}: "{self.name}" has data = {self._data}'
