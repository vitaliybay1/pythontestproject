from abc import ABC, abstractmethod
from SimpleObserverPattern.Publishers.Models import Publisher


class Observer(ABC):
    @abstractmethod
    def notify(self, publisher: Publisher):
        """Method that receive data from publisher"""
