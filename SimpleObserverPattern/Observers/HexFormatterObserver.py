from .Models.Observer import Observer
from SimpleObserverPattern.Publishers.Models.Publisher import Publisher

class HexFormatterObserver(Observer):

    def notify(self, publisher: Publisher):
        value = hex(publisher.data)
        print(f'{type(self).__name__}: "{publisher.name}" has  now hex data = {value}')
