from .Models.Observer import Observer
from SimpleObserverPattern.Publishers.Models import Publisher

class BinaryFormatterObserver(Observer):

    def notify(self, publisher: Publisher):
        value = bin(publisher.data)
        print(f'{type(self).__name__}: "{publisher.name}" has now binary data = {value}')
