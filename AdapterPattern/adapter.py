class Adapter:
    def __init__(self, obj, adapter_method):
        self.obj = obj
        self.__dict__.update(adapter_method)

    def __str__(self):
        return str(self.obj)
