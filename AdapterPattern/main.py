from AdapterPattern.Models.musician import Musician
from AdapterPattern.Models.dancer import Dancer
from AdapterPattern.adapter import Adapter
from AdapterPattern.club import Club


def main():
    objects = [Club("Jazz"), Musician("SomeName"), Dancer("SomeName")]
    for obj in objects:
        if hasattr(obj, 'play') or hasattr(obj, 'dance'):
            if hasattr(obj, 'play'):
                adapter_method = dict(organize_event=obj.play)
            if hasattr(obj, 'dance'):
                adapter_method = dict(organize_event=obj.dance)
            obj = Adapter(obj, adapter_method)
        print(f'{obj} {obj.organize_event()}')

if __name__ == "__main__":
    main()