class Musician:

    def __init__(self, name: str):
        self.name: str = name

    def __str__(self):
        return f'The musician {self.name}'

    def play(self):
        return 'plays music'
