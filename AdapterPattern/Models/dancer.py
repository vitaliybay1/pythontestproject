class Dancer:

    def __init__(self, name: str):
        self.name: str = name

    def __str__(self):
        return f'the dancer {self.name}'

    def dance(self):
        return 'does a dance performence'
