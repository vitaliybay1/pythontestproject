class Club:
    def __init__(self, name: str):
        self.name: str = name

    def __str__(self):
        return f'The club {self.name}'

    def organize_event(self):
        return 'hires an artist to perform for the people!'
