from FacadePattern.OperatingSystem import OperatingSystem

if __name__ == "__main__":
    os = OperatingSystem()
    os.start()
    os.create_file("user_test", "name_test", "super_permission")
    os.create_process("name_test", "user_test")
    os.stop()
