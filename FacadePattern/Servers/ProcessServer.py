from FacadePattern.Servers.Server import Server, State


class ProcessServer(Server):

    def __init__(self):
        self.name = "Process Server"
        self.state = State.new

    def boot(self):
        print(f"booting the {self}")
        self.state = State.running

    def kill(self, restart=True):
        print(f"Killing {self}")
        self.state = State.restart if restart else State.zombie

    def create_process(self, name, user):
        print(f"trying to create process '{name}' for user '{user}'")
