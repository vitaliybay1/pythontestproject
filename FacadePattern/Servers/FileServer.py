from FacadePattern.Servers.Server import Server, State


class FileServer(Server):

    def __init__(self):
        self.name = "File Server"
        self.state = State.new

    def boot(self):
        print(f"booting the {self}")
        self.state = State.running

    def kill(self, restart=True):
        print(f"killing {self}")
        self.state = State.restart if restart else State.zombie

    def create_file(self, user, name, permission):
        print(f"trying to create the file '{name}' for user '{user}' with permission '{permission}'")
