from abc import abstractmethod
from enum import Enum

State = Enum("State", "new running sleeping restart zombie")


class Server:

    @abstractmethod
    def __init__(self):
        pass

    def __str__(self):
        return self.name

    @abstractmethod
    def boot(self):
        pass

    @abstractmethod
    def kill(self, restart=True):
        pass