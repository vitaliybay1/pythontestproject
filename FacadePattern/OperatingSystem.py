from FacadePattern.Servers.FileServer import FileServer
from FacadePattern.Servers.ProcessServer import ProcessServer


class OperatingSystem:

    def __init__(self):
        self.file_server = FileServer()
        self.process_server = ProcessServer()

    def start(self):
        [i.boot() for i in (self.file_server, self.process_server)]

    def stop(self):
        [i.kill() for i in (self.file_server, self.process_server)]

    def create_file(self, user, name, permission):
        self.file_server.create_file(user, name, permission)

    def create_process(self, name, user):
        self.process_server.create_process(name, user)
