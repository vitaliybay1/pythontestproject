from typing import List
from StrategyPattern.SuportTicket import SupportTicket
from StrategyPattern.TicketOrderingStrategy import TicketOrderingStrategy


class FIFOOrderingStrategy(TicketOrderingStrategy):
    def createOrdering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        return list

