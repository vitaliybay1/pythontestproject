from typing import List
from StrategyPattern.SuportTicket import SupportTicket
from StrategyPattern.TicketOrderingStrategy import TicketOrderingStrategy


class FILOOrderingStrategy(TicketOrderingStrategy):
    def createOrdering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        listCopy = list.copy()
        listCopy.reverse()
        return listCopy
