from abc import ABC, abstractmethod
from typing import List
from StrategyPattern.SuportTicket import SupportTicket


class TicketOrderingStrategy(ABC):
    @abstractmethod
    def createOrdering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        pass
