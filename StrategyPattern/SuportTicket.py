import string
import random


def generateId(length=9):
    return ''.join(random.choices(string.ascii_letters, k=length))

class SupportTicket:
    id: str
    customer: str
    issue: str

    def __init__(self, customer: str, issue: str):
        self.id = generateId()
        self.customer = customer
        self.issue = issue

    def __repr__(self):
        return f"SupportTicket(id: {self.id}, customer: {self.customer}, issue: {self.issue})"