import random
from typing import List

from StrategyPattern.SuportTicket import SupportTicket
from StrategyPattern.TicketOrderingStrategy import TicketOrderingStrategy


class RandomOrderingStrategy(TicketOrderingStrategy):
    def createOrdering(self, list: List[SupportTicket]) -> List[SupportTicket]:
        listCopy = list.copy()
        random.shuffle(listCopy)
        return listCopy