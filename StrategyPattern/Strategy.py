from typing import List
from StrategyPattern.FIFOOrderingStrategy import FIFOOrderingStrategy
from StrategyPattern.FILOOrderingStrategy import FILOOrderingStrategy
from StrategyPattern.RandomOrderingStrategy import RandomOrderingStrategy
from StrategyPattern.SuportTicket import SupportTicket
from StrategyPattern.TicketOrderingStrategy import TicketOrderingStrategy


class CustomerSupport:
    tickets: List[SupportTicket] = []

    def createTicket(self, customer: str, issue: str):
        self.tickets.append(SupportTicket(customer, issue))

    def processTickets(self, processingStrategy: TicketOrderingStrategy):
        if not len(self.tickets):
            print("There are not tickets to process. Well done!")
            return

        ticketsOrdered = processingStrategy.createOrdering(self.tickets)

        for ticket in ticketsOrdered:
            print(ticket.__repr__())

app = CustomerSupport()

app.createTicket("User1", "Issue1")
app.createTicket("User2", "Issue2")
app.createTicket("User3", "Issue3")
app.createTicket("User4", "Issue4")

app.processTickets(RandomOrderingStrategy())

