import os
import yaml
import json


def transformFile(root: str, file: str, matchedFile: str):
    fileName = file.split(".yml")[0]
    with open(matchedFile) as f:
        data = json.dumps(yaml.load(f))
    with open(f"{root}\\{fileName}.json", 'w') as fpo:
        json.dump(data, fpo, indent=2)

for root, dirs, files in os.walk("V:\PythonTest\Test\YamlToJson"):
    for file in files:
        if file.endswith(".yml"):
             transformFile(root, file, os.path.join(root, file))
