from Decorators.Timed.TimedDecorator import timed


@timed
def main():
    total = 0
    # for n in range(1, 100001):
    #     total += n
    total = sum(range(1, 100001))
    print(total)
    return total


if __name__ == '__main__':
    main()
