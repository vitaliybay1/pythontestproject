import time


def timed(function):
    def wrapper(*args, **kwargs):
        begin = time.time()
        function_result = function(*args, **kwargs)
        end = time.time()
        function_name = function.__name__
        print(f'{function_name} took {end - begin} seconds to execute')
        return function_result

    return wrapper
