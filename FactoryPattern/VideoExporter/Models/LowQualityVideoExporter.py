import pathlib
from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter


class LowQualityVideoExporter(IVideoExporter):

    def prepare_export(self, video_data):
        print("Preparing low quality video data for exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting low quality video to {folder} folder")
