from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter
import pathlib

class MediumQualityVideoExporter(IVideoExporter):

    def prepare_export(self, video_data):
        print("Preparing medium quality video data")

    def do_export(self, folder: pathlib.Path):
        print(f"Export medium quality video to {folder} folder")
