from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter
import pathlib


class HighQualityVideoExporter(IVideoExporter):

    def prepare_export(self, video_data):
        print(f"Preparing high quality video data")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting high quality video to {folder} folder")
