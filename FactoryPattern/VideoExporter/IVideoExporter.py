import pathlib
from abc import ABC, abstractmethod


class IVideoExporter(ABC):
    """Representation of video exporting code"""

    @abstractmethod
    def prepare_export(self, video_data):
        """Prepare video for exporting"""

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """Exports the video data to a folder"""
