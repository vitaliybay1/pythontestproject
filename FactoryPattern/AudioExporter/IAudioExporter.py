from abc import ABC, abstractmethod
import pathlib


class IAudioExporter(ABC):

    @abstractmethod
    def prepare_export(self, audio_data):
        """Prepare audio for exporting """

    @abstractmethod
    def do_export(self, folder: pathlib.Path):
        """Export the audio to the folder"""
