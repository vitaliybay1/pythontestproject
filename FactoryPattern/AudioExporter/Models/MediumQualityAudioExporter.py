from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
import pathlib


class MediumQualityAudioExporter(IAudioExporter):

    def prepare_export(self, audio_data):
        print("Preparing medium quality audio for exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting medium quality audio to {folder} folder")
