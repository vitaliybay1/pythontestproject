from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
import pathlib


class HighQualityAudioExporter(IAudioExporter):

    def prepare_export(self, audio_data):
        print("Preparing high quality audio for exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting high quality audio to {folder} folder")
