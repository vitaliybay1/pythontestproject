from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
import pathlib


class LowQualityAudioExporter(IAudioExporter):

    def prepare_export(self, audio_data):
        print("Preparing low quality audio for exporting")

    def do_export(self, folder: pathlib.Path):
        print(f"Exporting low quality video to {folder} folder")
