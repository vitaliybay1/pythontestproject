from FactoryPattern.FactoryExporter.IFactoryExporter import IFactoryExporter
from FactoryPattern.FactoryExporter.Models.HighQualityExporter import HighQualityExporter
from FactoryPattern.FactoryExporter.Models.LowQualityExporter import LowQualityExporter
from FactoryPattern.FactoryExporter.Models.MediumQualityExporter import MediumQualityExporter


def get_exporter() -> IFactoryExporter:
    factories = {
        "low": LowQualityExporter(),
        "medium": MediumQualityExporter(),
        "high": HighQualityExporter()
    }

    export_qualities: str
    while True:
        export_qualities = input(f"Enter desired output quality ({', '.join(str(key) for key, value in factories.items())}): ")
        if export_qualities in factories:
            break
        print(f"Unknown output quality option: {export_qualities}")

    return factories[export_qualities]


def main(export_factory: IFactoryExporter):
    video_exporter = export_factory.get_video_exporter()
    audio_exporter = export_factory.get_audio_exporter()

    video_exporter.prepare_export("video data")
    audio_exporter.prepare_export("audio data")

    video_exporter.do_export("usr/bin/path")
    audio_exporter.do_export("usr/bin/path")


if __name__ == "__main__":
    exporter = get_exporter()
    main(exporter)
