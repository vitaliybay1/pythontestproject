from abc import ABC, abstractmethod
from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter


class IFactoryExporter(ABC):

    @abstractmethod
    def get_video_exporter(self) -> IVideoExporter:
        """Video exporter"""

    @abstractmethod
    def get_audio_exporter(self) -> IAudioExporter:
        """Audio Exporter"""
