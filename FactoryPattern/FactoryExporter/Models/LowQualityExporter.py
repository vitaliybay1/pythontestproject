from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
from FactoryPattern.AudioExporter.Models.LowQualityAudioExporter import LowQualityAudioExporter
from FactoryPattern.FactoryExporter.IFactoryExporter import IFactoryExporter
from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter
from FactoryPattern.VideoExporter.Models.LowQualityVideoExporter import LowQualityVideoExporter


class LowQualityExporter(IFactoryExporter):

    def get_video_exporter(self) -> IVideoExporter:
        return LowQualityVideoExporter()

    def get_audio_exporter(self) -> IAudioExporter:
        return LowQualityAudioExporter()
