from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
from FactoryPattern.AudioExporter.Models.HighQualityAudioExporter import HighQualityAudioExporter
from FactoryPattern.FactoryExporter.IFactoryExporter import IFactoryExporter
from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter
from FactoryPattern.VideoExporter.Models.HighQualityVideoExporter import HighQualityVideoExporter


class HighQualityExporter(IFactoryExporter):

    def get_video_exporter(self) -> IVideoExporter:
        return HighQualityVideoExporter()

    def get_audio_exporter(self) -> IAudioExporter:
        return HighQualityAudioExporter()
