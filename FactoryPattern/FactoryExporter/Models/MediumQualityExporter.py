from FactoryPattern.AudioExporter.IAudioExporter import IAudioExporter
from FactoryPattern.AudioExporter.Models.MediumQualityAudioExporter import MediumQualityAudioExporter
from FactoryPattern.FactoryExporter.IFactoryExporter import IFactoryExporter
from FactoryPattern.VideoExporter.IVideoExporter import IVideoExporter
from FactoryPattern.VideoExporter.Models.MediumQualityVideoExporter import MediumQualityVideoExporter


class MediumQualityExporter(IFactoryExporter):

    def get_audio_exporter(self) -> IAudioExporter:
        return MediumQualityAudioExporter()

    def get_video_exporter(self) -> IVideoExporter:
        return MediumQualityVideoExporter()
