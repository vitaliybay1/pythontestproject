"""
VitaliyCoin (VC)
"""
import hashlib


class VitaliyCoinBlock:

    def __init__(self, previus_block_hash: str, transaction_list: list):
        self.previus_block_hash = previus_block_hash
        self.transaction_list = transaction_list

        self.block_data = '-'.join(transaction_list) + '-' + previus_block_hash
        self.block_hash = hashlib.sha256(self.block_data.encode()).hexdigest()


t1 = 'Jorge sends 1 VC to Maria'
t2 = 'Alvaro sends 2 VC to Jorge'
t3 = 'Maria sends 4.4 VC to Ariadna'
t4 = 'Jorge sends 1.5 VC to Ariadna'
t5 = 'Pedro sends 7 VC to Victoria'
t6 = 'Ariadna sends 3 VC to Alvaro'
t7 = 'Claudio sends 3 VC to Natalia'

first_block = VitaliyCoinBlock('InitString', [t1, t2, t3])
print(first_block.block_data)
print(first_block.block_hash)

second_block = VitaliyCoinBlock(first_block.block_hash, [t4, t5])
print(second_block.block_data)
print(second_block.block_hash)

third_block = VitaliyCoinBlock(second_block.block_hash, [t6, t7])
print(third_block.block_data)
print(third_block.block_hash)
